package Examen.View;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;

public class Principal extends javax.swing.JFrame {

    HiloProgreso hilo;

    public Principal() {
        initComponents();
        iniciarSplash();
        hilo = new HiloProgreso(progreso);
        hilo.start();
        Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();  
                    Dimension ventana = this.getSize();  
                    this.setLocation((pantalla.width - ventana.width) / 2, (pantalla.height - ventana.height) / 2); 
        this.setSize(300, 315);
        hilo = null;
        
         
                    
    }

    public void iniciarSplash() {
        this.getjProgressBar1().setBorderPainted(true);
        this.getjProgressBar1().setForeground(new Color(50, 50, 153, 100));
        this.getjProgressBar1().setStringPainted(true);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        progreso = new javax.swing.JProgressBar();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        jPanel1.setLayout(new java.awt.BorderLayout());

        progreso.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                progresoStateChanged(evt);
            }
        });
        jPanel1.add(progreso, java.awt.BorderLayout.CENTER);

        getContentPane().add(jPanel1, java.awt.BorderLayout.PAGE_END);

        jPanel2.setLayout(new java.awt.BorderLayout());

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Examen/View/Depositphotos_62795041-562x562.jpg"))); // NOI18N
        jPanel2.add(jLabel2, java.awt.BorderLayout.CENTER);

        getContentPane().add(jPanel2, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void progresoStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_progresoStateChanged
if (progreso.getValue()==100){
    this.dispose();
    inicio ini = new inicio();
    
    Dimension pantall = Toolkit.getDefaultToolkit().getScreenSize();  
    Dimension ventan = ini.getSize();  
    ini.setLocation((pantall.width - ventan.width) / 2, (pantall.height - ventan.height) / 2);
    ini.setVisible(true);
}
}//GEN-LAST:event_progresoStateChanged

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JProgressBar progreso;
    // End of variables declaration//GEN-END:variables

    public javax.swing.JProgressBar getjProgressBar1() {
        return progreso;
    }
}
